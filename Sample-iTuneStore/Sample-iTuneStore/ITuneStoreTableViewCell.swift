//
//  ITuneStoreTableViewCell.swift
//  Sample-iTuneStore
//
//  Created by Thanakrit Ratsami on 2/24/2559 BE.
//  Copyright © 2559 James LifeStudio. All rights reserved.
//

import UIKit

class ITuneStoreTableViewCell: UITableViewCell {
    
    @IBOutlet private weak var ituneStoreCollectionView: UICollectionView!

}

extension ITuneStoreTableViewCell {
    
    func setCollectionViewDataSourceDelegate<D: protocol<UICollectionViewDataSource, UICollectionViewDelegate>>(dataSourceDelegate: D, forRow row: Int) {
        
        let width = (CGRectGetWidth(ituneStoreCollectionView!.frame) - 32.0) / 2.80
        let layout = ituneStoreCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.itemSize = CGSizeMake(width, width)
        
        ituneStoreCollectionView.delegate = dataSourceDelegate
        ituneStoreCollectionView.dataSource = dataSourceDelegate
        ituneStoreCollectionView.tag = row
        ituneStoreCollectionView.setContentOffset(ituneStoreCollectionView.contentOffset, animated:false)
        ituneStoreCollectionView.reloadData()
    }
    
    var collectionViewOffset: CGFloat {
        set {
            ituneStoreCollectionView.contentOffset.x = newValue
            
        }
        
        get {
            return ituneStoreCollectionView.contentOffset.x
        }
    }
    
}