//
//  ITuneStoreModels.swift
//  Sample-iTuneStore
//
//  Created by Thanakrit Ratsami on 2/24/2559 BE.
//  Copyright © 2559 James LifeStudio. All rights reserved.
//

import Foundation
import UIKit

class ITuneStoreModels {
    
    private var iTuneStoreModels = [ITuneStoreModel]()
    private var kinds = [String]()
    
    // MARK: - Public
    
    var numberOfITuneModels: Int {
        return iTuneStoreModels.count
    }
    
    var numberOfKinds: Int {
        return kinds.count
    }
    
    init()
    {
        iTuneStoreModels = createITuneStoreModels()
        kinds = ["Rock", "Pop", "Dance"]
    }
    
    func numberOfITuneStoreModelsInSection(index: Int) -> Int {
        let iTuneStoreModel = ituneStoreModelsForSection(index)
        return iTuneStoreModel.count
    }
    
    func iTuneStoreModelForItemAtIndexPath(indexPath: NSIndexPath) -> ITuneStoreModel? {
        if indexPath.section > 0 {
            let iTuneStoreModels = ituneStoreModelsForSection(indexPath.section)
            return iTuneStoreModels[indexPath.item]
        } else {
            return iTuneStoreModels[indexPath.item]
        }
    }
    
    func titleForSectionAtIndexPath(indexPath: Int) -> String?
    {
        if indexPath < kinds.count {
            return kinds[indexPath]
        }
        return nil
    }
    
    
    
    // MARK: - Private
    
    private func createITuneStoreModels() -> [ITuneStoreModel]
    {
        var newITuneStoreModels = [ITuneStoreModel]()
        newITuneStoreModels += KindRock.iTuneStoreModels()
        newITuneStoreModels += KindPop.iTuneStoreModels()
        newITuneStoreModels += KindDance.iTuneStoreModels()
        return newITuneStoreModels
    }
    
    private func ituneStoreModelsForSection(index: Int) -> [ITuneStoreModel] {
        let kind = kinds[index]
        let iTuneStoreModelsInKind = iTuneStoreModels.filter {
            (iTuneStoreModel: ITuneStoreModel) -> Bool in return iTuneStoreModel.kind == kind
        }
        return iTuneStoreModelsInKind
    }
    
}

class KindRock
{
    class func iTuneStoreModels() -> [ITuneStoreModel]
    {
        var iTuneStoreModel = [ITuneStoreModel]()
        iTuneStoreModel.append(ITuneStoreModel(name: "Name", band: "Band", cover: UIImage(named: "cover-bodyslam")!, kind: "Rock"))
        iTuneStoreModel.append(ITuneStoreModel(name: "Name", band: "Band", cover: UIImage(named: "cover-bodyslam")!, kind: "Rock"))
        iTuneStoreModel.append(ITuneStoreModel(name: "Name", band: "Band", cover: UIImage(named: "cover-bodyslam")!, kind: "Rock"))
        iTuneStoreModel.append(ITuneStoreModel(name: "Name", band: "Band", cover: UIImage(named: "cover-bodyslam")!, kind: "Rock"))
        iTuneStoreModel.append(ITuneStoreModel(name: "Name", band: "Band", cover: UIImage(named: "cover-bodyslam")!, kind: "Rock"))
        return iTuneStoreModel
    }
}

class KindPop
{
    class func iTuneStoreModels() -> [ITuneStoreModel]
    {
        var iTuneStoreModel = [ITuneStoreModel]()
        iTuneStoreModel.append(ITuneStoreModel(name: "Name", band: "Band", cover: UIImage(named: "cover-sweetmullet")!, kind: "Pop"))
        iTuneStoreModel.append(ITuneStoreModel(name: "Name", band: "Band", cover: UIImage(named: "cover-sweetmullet")!, kind: "Pop"))
        iTuneStoreModel.append(ITuneStoreModel(name: "Name", band: "Band", cover: UIImage(named: "cover-sweetmullet")!, kind: "Pop"))
        iTuneStoreModel.append(ITuneStoreModel(name: "Name", band: "Band", cover: UIImage(named: "cover-sweetmullet")!, kind: "Pop"))
        iTuneStoreModel.append(ITuneStoreModel(name: "Name", band: "Band", cover: UIImage(named: "cover-sweetmullet")!, kind: "Pop"))
        return iTuneStoreModel
    }
}

class KindDance
{
    class func iTuneStoreModels() -> [ITuneStoreModel]
    {
        var iTuneStoreModel = [ITuneStoreModel]()
        iTuneStoreModel.append(ITuneStoreModel(name: "Name", band: "Band", cover: UIImage(named: "cover-retrospect")!, kind: "Dance"))
        iTuneStoreModel.append(ITuneStoreModel(name: "Name", band: "Band", cover: UIImage(named: "cover-retrospect")!, kind: "Dance"))
        iTuneStoreModel.append(ITuneStoreModel(name: "Name", band: "Band", cover: UIImage(named: "cover-retrospect")!, kind: "Dance"))
        iTuneStoreModel.append(ITuneStoreModel(name: "Name", band: "Band", cover: UIImage(named: "cover-retrospect")!, kind: "Dance"))
        iTuneStoreModel.append(ITuneStoreModel(name: "Name", band: "Band", cover: UIImage(named: "cover-retrospect")!, kind: "Dance"))
        return iTuneStoreModel
    }
}

