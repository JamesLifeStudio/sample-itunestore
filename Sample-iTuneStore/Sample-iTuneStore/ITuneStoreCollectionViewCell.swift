//
//  ITuneStoreCollectionViewCell.swift
//  Sample-iTuneStore
//
//  Created by Thanakrit Ratsami on 2/24/2559 BE.
//  Copyright © 2559 James LifeStudio. All rights reserved.
//

import UIKit

class ITuneStoreCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var bandLabel: UILabel!
    
    var iTuneStoreModel: ITuneStoreModel? {
        didSet {
            updateUI()
        }
    }
    
    func updateUI()
    {
        coverImageView.image = iTuneStoreModel?.cover
        nameLabel.text = iTuneStoreModel?.name
        bandLabel.text = iTuneStoreModel?.band
    }
    
}
