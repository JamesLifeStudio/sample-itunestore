//
//  ITuneStoreModel.swift
//  Sample-iTuneStore
//
//  Created by Thanakrit Ratsami on 2/24/2559 BE.
//  Copyright © 2559 James LifeStudio. All rights reserved.
//

import UIKit

class ITuneStoreModel {
    var name: String
    var band: String
    var cover: UIImage
    var kind: String
    
    init(name: String, band: String, cover: UIImage, kind: String)
    {
        self.name = name
        self.band = band
        self.cover = cover
        self.kind = kind
    }
    
    convenience init(copies iTuneStoreModel: ITuneStoreModel)
    {
        self.init(name: iTuneStoreModel.name, band: iTuneStoreModel.band, cover: iTuneStoreModel.cover, kind: iTuneStoreModel.kind)
    }
}