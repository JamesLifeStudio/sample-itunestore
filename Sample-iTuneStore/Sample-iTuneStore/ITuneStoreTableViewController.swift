//
//  ITuneStoreController.swift
//  Sample-iTuneStore
//
//  Created by Thanakrit Ratsami on 2/24/2559 BE.
//  Copyright © 2559 James LifeStudio. All rights reserved.
//

import UIKit

class ITuneStoreTableViewController: UITableViewController {
    
    let iTuneStoreModels = ITuneStoreModels()
    
    var storedOffsets = [Int: CGFloat]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        //        let width = (CGRectGetWidth(freelanceCollectionView!.frame) - 32.0) / 2.80
        //        let layout = freelanceCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
        //        layout.itemSize = CGSizeMake(width, width)
        
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private struct Storyboard
    {
        static let ITuneStoreCollectionViewCellIdentifier = "ITuneStoreCollectionViewCell"
        static let ITuneStoreTableViewCellIdentifier = "ITuneStoreTableViewCell"
    }
    
    // MARK: UITableViewDataSource
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return iTuneStoreModels.numberOfKinds
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier(Storyboard.ITuneStoreTableViewCellIdentifier, forIndexPath: indexPath) as! ITuneStoreTableViewCell
        
        return cell
    }
    
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
        guard let iTuneStoreTableViewCell = cell as? ITuneStoreTableViewCell else { return }
        
        iTuneStoreTableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
        iTuneStoreTableViewCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
    }
    
    override func tableView(tableView: UITableView, didEndDisplayingCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
        guard let freelanceTableViewCell = cell as? ITuneStoreTableViewCell else { return }
        
        storedOffsets[indexPath.row] = freelanceTableViewCell.collectionViewOffset
    }
    
    // MARK: UITableViewDelegate
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let kind = iTuneStoreModels.titleForSectionAtIndexPath(section)
        return kind
    }

}

extension ITuneStoreTableViewController: UICollectionViewDelegate, UICollectionViewDataSource {

    // MARK: UICollectionViewDataSource
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return iTuneStoreModels.numberOfITuneStoreModelsInSection(section)
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(Storyboard.ITuneStoreCollectionViewCellIdentifier, forIndexPath: indexPath) as! ITuneStoreCollectionViewCell
        cell.iTuneStoreModel = iTuneStoreModels.iTuneStoreModelForItemAtIndexPath(indexPath);
        
        return cell
    }
    
    // MARK: UICollectionViewDelegate
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        print("Collection view at row \(collectionView.tag) selected index path \(indexPath)")
    }

}
